import collections
import math

import numpy as np
from mediapipe.framework.formats import landmark_pb2

LEFT_HAND_LANDMARKS = 0
RIGHT_HAND_LANDMARKS = 1
POSE_LANDMARKS = 2
POSE_WORLD_LANDMARKS = 3
FACE_LANDMARKS = 4
SEGMENTATION_MASK = 5

RESULTS_HIERARCHY = {
    LEFT_HAND_LANDMARKS: "left_hand_landmarks",
    RIGHT_HAND_LANDMARKS: "right_hand_landmarks",
    POSE_LANDMARKS: "pose_landmarks",
    POSE_WORLD_LANDMARKS: "pose_world_landmarks",
    FACE_LANDMARKS: "face_landmarks",
    SEGMENTATION_MASK: "segmentation_mask"
}


def results_to_numpy(results):
    # .x .y .z .visibility
    lh = results.left_hand_landmarks
    rhl = results.right_hand_landmarks
    pl = results.pose_landmarks
    pwl = results.pose_world_landmarks
    fl = results.face_landmarks

    lhl = np.asarray([np.array([e.x, e.y, e.z, e.visibility], dtype=np.float128) for e in lh.landmark]) if lh else None
    rhl = np.asarray([np.array([e.x, e.y, e.z, e.visibility], dtype=np.float128) for e in rhl.landmark]) if rhl else None
    pl = np.asarray([np.array([e.x, e.y, e.z, e.visibility], dtype=np.float128) for e in pl.landmark]) if pl else None
    pwl = np.asarray([np.array([e.x, e.y, e.z, e.visibility], dtype=np.float128) for e in pwl.landmark]) if pwl else None
    fl = np.asarray([np.array([e.x, e.y, e.z, e.visibility], dtype=np.float128) for e in fl.landmark]) if fl else None
    segmentation_mask = results.segmentation_mask

    return np.array([lhl, rhl, pl, pwl, fl, segmentation_mask], dtype=object)


def numpy_to_landmarks(video_np_landmark_results):
    results_list = []
    for frame_i, frame_results in enumerate(video_np_landmark_results):
        solution_outputs = collections.namedtuple('SolutionOutputs', list(RESULTS_HIERARCHY.values()))
        for idx, lmx_category_values in enumerate(frame_results):
            category_name = RESULTS_HIERARCHY[idx]
            lmx_list_parent = landmark_pb2.NormalizedLandmarkList()
            if lmx_category_values is None:
                setattr(solution_outputs, category_name, None)
                continue
            for v in lmx_category_values:
                lmx_list_parent.landmark.add(x=v[0], y=v[1], z=v[2])
            setattr(solution_outputs, category_name, lmx_list_parent)
        results_list.append(solution_outputs)
    return results_list


def get_2d_angle(p1, p2, p3):
    rad = np.arctan2(p3.y - p2.y, p3.x - p2.x) - np.arctan2(p1.y - p2.y, p1.x - p2.x)
    return np.abs(np.rad2deg(rad))


def get_3d_angle(p1, p2, p3):
    v1 = np.array([p1.x - p2.x, p1.y - p2.y, p1.z - p2.z])
    v2 = np.array([p3.x - p2.x, p3.y - p2.y, p3.z - p2.z])

    v1mag = np.sqrt([v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2]])
    v1norm = np.array([v1[0] / v1mag, v1[1] / v1mag, v1[2] / v1mag])

    v2mag = np.sqrt(v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2])
    v2norm = np.array([v2[0] / v2mag, v2[1] / v2mag, v2[2] / v2mag])
    res = v1norm[0] * v2norm[0] + v1norm[1] * v2norm[1] + v1norm[2] * v2norm[2]
    angle_rad = np.arccos(res)

    return np.abs(np.rad2deg(angle_rad))[0]


def get_2d_distance(p1, p2):
    return math.hypot(p2.x - p1.x, p2.y - p1.y)


def get_3d_distance(p1, p2):
    return np.linalg.norm(np.asarray([p1.x, p1.y, p1.z]) - np.asarray([p2.x, p2.y, p2.z]))
