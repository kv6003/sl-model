import os

import cv2.cv2 as cv2
import mediapipe as mp
import numpy as np

DATASET_ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath("__file__")), "czech_sign_language/")


def get_video_path(word_class, idx):
    word_class_directory = os.path.join(DATASET_ROOT_DIR, word_class)
    video_sample_path = os.path.join(word_class_directory, f"{idx}.avi")
    return video_sample_path, word_class_directory


def results_to_numpy(results):
    # .x .y .z .visibility
    lh = results.left_hand_landmarks
    rhl = results.right_hand_landmarks
    pl = results.pose_landmarks
    pwl = results.pose_world_landmarks
    fl = results.face_landmarks

    lhl = np.asarray([np.array([e.x, e.y, e.z, e.visibility], dtype=np.float128) for e in lh.landmark]) if lh else None
    rhl = np.asarray([np.array([e.x, e.y, e.z, e.visibility], dtype=np.float128) for e in rhl.landmark]) if rhl else None
    pl = np.asarray([np.array([e.x, e.y, e.z, e.visibility], dtype=np.float128) for e in pl.landmark]) if pl else None
    pwl = np.asarray([np.array([e.x, e.y, e.z, e.visibility], dtype=np.float128) for e in pwl.landmark]) if pwl else None
    fl = np.asarray([np.array([e.x, e.y, e.z, e.visibility], dtype=np.float128) for e in fl.landmark]) if fl else None
    segmentation_mask = results.segmentation_mask

    return np.array([lhl, rhl, pl, pwl, fl, segmentation_mask], dtype=object)


def main():
    holistic = mp.solutions.holistic.Holistic(
        model_complexity=2,
        refine_face_landmarks=True,
    )
    classes = [f.name for f in os.scandir(DATASET_ROOT_DIR) if f.is_dir()]

    for cls in classes:
        print(cls)
        for i in range(30):
            print(i)
            sample_dir, base_dir = get_video_path(cls, i)
            res_array = []
            cap = cv2.VideoCapture(sample_dir)
            while cap.isOpened():
                ret, frame = cap.read()
                if not ret:
                    cap.release()
                    break
                frame_results = holistic.process(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
                np_results = results_to_numpy(frame_results)
                res_array.append(np_results)
            np.save(os.path.join(base_dir, f"{i}.npy"), np.asarray(res_array))


if __name__ == "__main__":
    main()
