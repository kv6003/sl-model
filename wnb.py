sweep_config = {
    "method": "grid",     # random, grid, bayes
    "metric": {
        "name": "val_loss",
        "goal": "minimize"
    },
    "parameters": {
        "epoch": {
            "values": [100, 150, 250]
        },
        "optimizer": {
            "values": ["adam", "sgd"]
        },
        "dropout": {
            "values": [0.3, 0.4, 0.5]
        },
        "learning_rate": {
            "values": [0.0005, 0.001, 0.005]
            # "distribution": "uniform",
            # "min": 0.0001,
            # "max": 0.01
        }
    }
}
